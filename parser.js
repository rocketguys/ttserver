var request = require('request');
var routes = require('./routes');
var Bus = require('./Bus');
var Station = require('./Station');
var numOfBus = [2,6,7,8,9,10,11,12,15,16,18,19,21,22,23,25,26,27,28,29,31,34,36,39,40,41,45,49,50,52,53,55,56,68,71,79]
var N=numOfBus.length;
var start=-2;
var getNextBusStation = function(body,sc){
	start = body.indexOf("[station=");
	if(body.substring(start,body.length).indexOf("<a")==-1 || start==-1)
		return -1;
	start+=9;
	var num = body[start];
	while(body[++start]!=']')
		num+=body[start];
	//Убираем букву n
	if(num[num.length-1]=='n'){
		num=num.substring(0,num.length-1);
		for(var j=0;j<routes[numOfBus[sc]].length;j++){
			if(routes[numOfBus[sc]][j]==num){
				num=routes[numOfBus[sc]][(j+1)%routes[numOfBus[sc]].length];
				break;
			}
		}
	}
	return num;
}

var getIdOfBus = function(body){
	start = body.indexOf("station='")+9;
	var idOfBus = body[start];
	while(body[++start]!="'")
		idOfBus+=body[start];
	return idOfBus;
}

var getNextTime = function(tempBody){
	var ind = tempBody.indexOf("grey>")+5;
	var timeFrom = tempBody[ind];
	while(tempBody[++ind]!="&")
		timeFrom+=tempBody[ind];
	return timeFrom;
}

var getStationName = function(body){
	start=body.indexOf("Идёт до ост. ") + 13;
	var station=body[start];
	while(body[++start]!="'")
		station+=body[start];
	return station;
}

var updateBusDB = function(scMDB,sc,routeBuses){
	Station.findOne({id:routes[numOfBus[sc]][scMDB]},function(err,station){
		var isBus = ((routeBuses[scMDB]==undefined)?false:true);
		if(station==null){
			if(isBus){
				station = new Station({
					id:routes[numOfBus[sc]][scMDB],
					buses: [routeBuses[scMDB]]
				})
				station.save(function(err){
					console.log(err);
					if(scMDB!=routes[numOfBus[sc]].length-1)
						updateBusDB(scMDB+1,sc,routeBuses);
				});
			}
		}
		else{
			if(isBus){
				var isHere=false;
				for(var j=0;j<station.buses.length;j++){
					if(station.buses[j].number==routeBuses[scMDB].number){
						station.buses[j]=routeBuses[scMDB];
						isHere=true;
						break;
					}
				}
				if(!isHere)
					station.buses.push(routeBuses[scMDB]);
				station.buses.sort(function(bus1,bus2){
					return parseInt(bus1.timeFrom)>parseInt(bus2.timeFrom);
				})
				Station.update({id:routes[numOfBus[sc]][scMDB]},{buses:station.buses},function(err){
					//console.log("#Update",routes[numOfBus[sc]][scMDB]);
					if(scMDB!=routes[numOfBus[sc]].length-1)
						updateBusDB(scMDB+1,sc,routeBuses);
				})
			}
			else{
				var index=-1;
				for(var i=0;i<station.buses.length;i++){
					if(station.buses[i].number==numOfBus[sc]){
						index = i;
						station.buses.splice(i,1);
						break;
					}
				}
				Station.update({id:routes[numOfBus[sc]][scMDB]},{buses:station.buses},function(err){
					//console.log("#Delete",numOfBus[sc],routes[numOfBus[sc]][scMDB]);
					if(scMDB!=routes[numOfBus[sc]].length-1)
						updateBusDB(scMDB+1,sc,routeBuses);
				})
			}
		}
	})
}

var parse = function(){
	var sc=0;
	var requests = [];
	for(var SC=0;SC<N;SC++){
		requests.push(function(){
			request('http://igis.ru/com/gortrans/page/online.php?nom='+numOfBus[sc]+'&mode=1&rnd=83617',function(err,res,body){
				if(!err){
					var idOfBuses = [];
					var buses = [];
					var routeBuses = [];
					var stationNames = [];
					while(1){
						//Находим автобус
						var num=getNextBusStation(body,sc);
						if(num==-1)
							break;
						body = body.substring(start,body.length);
						//Получаем его id
						var idOfBus = getIdOfBus(body);
						//Пихаем все в массивы
						idOfBuses.push(idOfBus);
						buses.push(num);
						//Обрезаем body
						body = body.substring(start,body.length);
					}
					start=-2;
					if(buses.length==0){
						updateBusDB(0,sc,routeBuses);
					}
					//Проходимся по всем остановкам
					// console.log(routes[numOfBus[sc]].length);
					for(var i=0;i<buses.length;i++){
						//Номер текущей остановки
						var index = routes[numOfBus[sc]].indexOf(buses[i]),flag=1;
						while(flag==1 || (buses.indexOf(routes[numOfBus[sc]][index])==-1 && routeBuses[index]==undefined)){
							var tempBody = body.substring(body.indexOf("[station="+routes[numOfBus[sc]][index]),body.length);
							//Получаем время и добавляем его в массив
							if(tempBody!=body)
								var timeFrom=getNextTime(tempBody);
							else
								var timeFrom='0';
							routeBuses[index]=new Bus(numOfBus[sc],idOfBuses[i],buses[i],timeFrom);
							// console.log(index);
							index = (index+1)%routes[numOfBus[sc]].length;
							flag=0;
						}
						if(i==buses.length-1)
							updateBusDB(0,sc,routeBuses);
					}
					sc=(sc+1)%N;
				}
				setTimeout(function(){
					requests[sc]();
				},1000);
			})
		})
	}
	requests[0]();
}

module.exports = parse;