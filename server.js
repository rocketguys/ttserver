'use strict'
var app = require('express')();
var Station = require('./Station');
var routes = require('./routes');
var events = require('events');
var em;
var response;
var sc;
var size;
var buses;

class myBus{
	constructor(dist,num,station){
		this.dist=dist;
		this.num=num;
		this.station=station;
	}
}

var getDist = function(from,to,numOfBus){
	from=routes[numOfBus].indexOf(from);
	to=routes[numOfBus].indexOf(to);
	if(to>from)
		return to-from;
	return to+routes[numOfBus].length-from;
}

var prepareResponse = function(i){
	Station.findOne({id:buses[i].station},function(err,station){
		for(var bus of station.buses){
			if(bus.number == buses[i].num){
				response["buses"].push(bus);
				break;
			}
		}
		sc++;
		if(sc==size.length)
			em.emit("continue");
	})
}

app.get('/getTransport', function(req,res){
	em=new events.EventEmitter();
	size = [];
	sc=0;
	buses = [];
	response = {"buses":[]};
	var from = req.query.stationFrom.substring(1,req.query.stationFrom.length-1).split(', ');
	var to = req.query.stationTo.substring(1,req.query.stationTo.length-1).split(', ');
	console.log(from);
	console.log(to);
	for(var i=0;i<from.length;i++){
		for(var j=0;j<to.length;j++){
			for(var route in routes){
				//console.log("1",size);
				if(routes[route].indexOf(from[i])!=-1 && routes[route].indexOf(to[j])!=-1){
					buses.push(new myBus(getDist(from[i],to[j],route),route,from[i]));
					if(size.indexOf(route)==-1){
						size.push(route);
					}
				}
			}
		}
	}
	buses.sort(function(bus1,bus2){
		var first = parseInt(bus1.dist);
		var second = parseInt(bus2.dist);
		if(first==second)
			return 0;
		else if(first>second)
			return 1;
		else return -1;
	});
	console.log(buses);
	for(var i=0;i<buses.length;i++){
		var used=false;
		for(var j=0;j<i;j++){
			if(buses[i].num==buses[j].num){
				used=true;
				break;
			}
		}
		if(!used){
			prepareResponse(i);
		}
	}
	em.on("continue",function(){
		console.log("HUUUUI"); 
		response["buses"].sort(function(bus1,bus2){
			return parseInt(bus1.timeFrom)>(bus2.timeFrom);
		})
		res.json(response).end();
	})
})

var runServer = function(){
	app.listen(8080,function(){
		console.log("I'm listening!");
	})
}

module.exports = runServer;