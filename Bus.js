'use strict'
class Bus{
	constructor(number,id,station,timeFrom,timeTo,park){
		this.number=number;
		this.id=id;
		this.park=park||0;
		this.timeFrom=timeFrom;
		this.timeTo=timeTo;
		this.station=station;
	}
}
module.exports = Bus;