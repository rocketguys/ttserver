var mongoose = require('mongoose');

var Station = mongoose.model('station',{
	id: String,
	buses: [{
		number: String,
		id: String,
		park: String,
		timeFrom: String,
		timeTo: String,
		station: String
	}]
})

module.exports = Station;